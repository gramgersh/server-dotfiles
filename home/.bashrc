##
## Environment Variables
##

# notify of background jobs
set -o notify
set -o emacs
set -o ignoreeof

# Don't put duplicate lines in the history.
export HISTCONTROL="ignoredups"

# Ignore some controlling instructions
export HISTIGNORE="[   ]*:&:bg:fg:exit"

alias j='jobs -l'
alias ls='/bin/ls -F'
alias ll='ls -lh'
alias nil='/bin/cat >/dev/null'
alias pd=pushd
alias pg="ps -ef|grep"

# Don't actually want vi aliased.  Some times it is aliased by the
# system, so
alias vi=vim
unalias vi

export EXINIT='set ai wm=10 sw=4 showmatch hls!'

unset MAIL

case "$TERM" in
	xterms | xterm-* ) TERM=xterm ;;
esac

function ctime {
    perl -e 'foreach my $t (@ARGV) { print scalar localtime($t), "\n"; }' "$@"
}
function findfxg {
    find . \( -name .svn -o -name .snapshot -o -name .git \) -prune -o -type f -print0 | xargs -0 grep "$@";
}
function findfile {
    find . \( -name .svn -o -name .snapshot -o -name .git \) -prune -o -name "*$1*" -print 2>/dev/null;
}
function xless {
    xterm -n "less $*" -title "less $*" -name lessxterm -e less "$@" & 
}
function xman {
    xterm -n "man $*" -title "man $*" -name manxterm -e man "$@" & 
}
function xperldoc {
    xterm -n "perldoc $*" -title "perldoc $*" -name manxterm -e perldoc "$@" & 
}
function xssh { xterm -e ssh -X "$@" & }
function xswt { 
	unset PROMPT_COMMAND; echo -ne '\033]0;'$@'\007'
}
function xtelnet {
    xterm -n "telnet $*" -title "telnet $*" -name bluexterm -e telnet "$@" & 
}
function xvi {
    xterm -n "vi $@" -title "vi $@" -name vixterm -e vi "$@" & 
}
