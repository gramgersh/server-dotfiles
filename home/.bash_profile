#
#
stty erase ^?
umask 022

unset MAILCHECK
PS1='\h:\w($?)\$ '
export RSYNC_RSH=ssh
export GPG_TTY=$(tty)
export LC_ALL=C

set -o notify
set -o emacs
