# server-dotfiles

This contains root bashrc replacement files.

To install:

```bash
cd $HOME
curl -s https://bitbucket.org/gramgersh/server-dotfiles/get/master.tar.gz | \
	tar xzf - --strip-components=2 --suffix=.$(date "+%Y%m%d-%H%M") */home
```

